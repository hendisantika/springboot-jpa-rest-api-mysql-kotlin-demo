package com.hendisantika.springbootjparestapimysqlkotlindemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class SpringbootJpaRestApiMysqlKotlinDemoApplication

fun main(args: Array<String>) {
    runApplication<SpringbootJpaRestApiMysqlKotlinDemoApplication>(*args)
}
