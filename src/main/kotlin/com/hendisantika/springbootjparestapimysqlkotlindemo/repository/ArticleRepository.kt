package com.hendisantika.springbootjparestapimysqlkotlindemo.repository

import com.hendisantika.springbootjparestapimysqlkotlindemo.model.Article
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jpa-rest-api-mysql-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/02/20
 * Time: 05.43
 */

@Repository
interface ArticleRepository : JpaRepository<Article, Long>
