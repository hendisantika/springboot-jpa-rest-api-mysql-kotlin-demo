package com.hendisantika.springbootjparestapimysqlkotlindemo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.time.LocalDateTime

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jpa-rest-api-mysql-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/04/20
 * Time: 13.00
 */
@Controller
@RequestMapping("/")
class IndexController {
    @GetMapping
    fun index(model: Model): String {
        model.addAttribute("tanggal", LocalDateTime.now())
        return "index"
    }
}